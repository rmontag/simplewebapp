import static org.junit.Assert.assertEquals;

import java.net.MalformedURLException;
import java.net.URL;

import org.apache.log4j.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class TestRunner {

	private static final Logger log = Logger.getLogger(TestRunner.class);

	static WebDriver driver;

	@BeforeClass
	public static void initDriver() throws MalformedURLException {
		log.debug("Creating driver");

		String serverUrl = System.getProperty("grid.server.url");
		String gridServerUrl = "http://selenium-hub:4444/wd/hub";
		if (serverUrl != null) {
			gridServerUrl = serverUrl;
		}

		// chrome
		DesiredCapabilities capability = DesiredCapabilities.chrome();
		URL gridUrl = new URL(gridServerUrl);
		driver = new RemoteWebDriver(gridUrl, capability);
		log.debug("Driver created");
	}

	@AfterClass
	public static void quitDriver() {
		driver.quit();
	}

	@Test
	@Ignore
	public void testPing() throws Exception {
		WebDriver driver  = new FirefoxDriver();
		driver.navigate().to("http://localhost:8080/simplewebapp/index.jsp");
		String header1 = driver.findElement(By.xpath("//h1")).getText();
		assertEquals(header1, "PING2");
		driver.quit();
	}

	@Test
	public void testPingHub() throws Exception {

		String webappUrl = System.getProperty("webapp.server.url");
		String webappServerUrl = "http://172.18.0.10:8080";
		if (webappUrl != null) {
			webappServerUrl = webappUrl;
		}

		driver.navigate().to(webappServerUrl + "/simplewebapp/index.jsp");
		String header1 = driver.findElement(By.xpath("//h1")).getText();

		assertEquals(header1, "PING2");
	}
}
